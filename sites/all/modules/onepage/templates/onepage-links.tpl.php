<div id="onepage-links-wrapper">
	<ul id="onepage-links" class="onepage-links">
    <?php foreach($links as $section => $link): ?>
      <li><?php print l($link['text'], $link['path'], $link['options']) ?></li>
    <?php endforeach;?>	
	</ul>
</div>
