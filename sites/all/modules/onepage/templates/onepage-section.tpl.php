<div id="section-<?php print $name; ?>" class="onepage-section section">
  <div class="container">
      <div class="main">
			    <div class="section-title">
			      <h1><?php print $section['title']; ?><h1>
			    </div>
			    
			    <div class="section-content">
			      <?php print drupal_render($section['content']); ?>
			    </div>        
      </div>
  </div>
</div>
