<div id="wrapper-outer">
  <div id="wrapper-footer">
    <div id="wrapper">
        <div id="header">
          <div class="container">
	          <div class="main">
	             <?php if($site_slogan): ?>
	               <h1 class="mission"><?php print $site_slogan; ?></h1>
	             <?php endif; ?>        
	          </div>
            
            <div id="nav">
              <?php if ($logo): ?>
                <a id="logo" class="logo brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
                  <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
                </a>
              <?php endif; ?>
               
               <ul id="primary-links"></ul>
               
               <?php if ($page['sidebar_first']): ?>
                  <div id="sidebar-first"><?php print render($page['sidebar_first']); ?></div>
               <?php endif; ?>            
            </div>	          
	          
	          <div class="clear"></div>          
          </div>
        </div>
        
	      <div id="main-content"><?php print render($page['content']); ?></div>
	      
	      <div id="footer-illustration">
	         <?php print render($page['footer']); ?>
	         <p>Copyright 2012&copy;. All rights reserved.</p>
	       </div>           
    </div>
  </div>
</div>